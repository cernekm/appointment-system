
* Naklonovanie projektu:

```bash
  martin:~/Coding/VUT_FIT/ITU/appointment-system$ git clone git@gitlab.com:cernekm/appointment-system.git
```


## Spustenie:

* najprv treba vytvorit virtualne prostredie (treba nainstalovat virtualnenv)

```bash
martin:~/Coding/VUT_FIT/ITU/appointment-system$ virtualenv apenv
```


* prepnutie do virtualenv

```bash
martin:~/Coding/VUT_FIT/ITU/appointment-system$ source apenv/bin/activate
```

ak si to spravil dobre tak by si mal mat na zaciatku command lineu (apenv)frederik...

* nainstalovanie potrebnych balickov

```bash
(apenv)martin:~/Coding/VUT_FIT/ITU/appointment-system$ pip install -r requirements.txt
```


* spustenie

```bash
(apenv)martin:~/Coding/VUT_FIT/ITU/appointment-system$ python run.py
```

uvidis nieco taketo ->
```bash
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
 * Restarting with stat
```

* teraz mozes pristupovat na http://localhost:5000

* po skonceni prace deaktivujes virtualenv 

```bash
(apenv)martin:~/Coding/VUT_FIT/ITU/appointment-system$ deactivate
```



odteraz ked budes chciet pracovat na projekte tak sa len prepnes do virtualenv (source apenv/bin/activate), spustis a vsetko pojde ako ma, len raz za cas bude treba doinstalovat balicky ak pouzijeme nejake nestandardne

virtualenv sluzi na to aby si si nezasvinil notebook, vsetky suviace balicky sa instaluju do toho virtualenv a nie priamo do systemu, cize vsetko sa to nainstaluje do tej zlozky a ty ju mozes kedykolvek vymazat bez toho aby si nejak poskodil system





CSS ukladaj do zlozky AppointmentSys/static/, teda napriklad example.css

templaty(stranky) do  AppointmentSys/templates/

ked budes potrebovat pristupit k css tak z html to takto spravis 

```html
<link href="static/example.css" rel="stylesheet">
```




pomocou tohoto si nastavujes cesty
```python
@app.route('/')
def index():
    return render_template("index.html", title="Main Page")
```

snad to nejak pochopis, ak nie tak si mozme zavolat

