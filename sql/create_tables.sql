DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS login_data CASCADE;
DROP TABLE IF EXISTS consultations CASCADE;
DROP TABLE IF EXISTS registered_consultations CASCADE;
DROP TABLE IF EXISTS messages CASCADE;
DROP SEQUENCE IF EXISTS users_id_seq CASCADE;
DROP SEQUENCE IF EXISTS consultations_id_seq CASCADE;


CREATE SEQUENCE users_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE consultations_id_seq START WITH 1 INCREMENT BY 1;

CREATE TABLE users(
    id             INTEGER PRIMARY KEY    DEFAULT NEXTVAL('users_id_seq'),
    name           VARCHAR(256)           NOT NULL,
    surname        VARCHAR(256)           NOT NULL,
    date_of_birth  DATE                   NOT NULL,
    phone          VARCHAR(30)            NOT NULL,
    email          VARCHAR(256)           NOT NULL,
    type           VARCHAR(20)            NOT NULL,
    email_notf     BOOL                   DEFAULT TRUE,
    sms_notf       BOOL                   DEFAULT TRUE,
    active         BOOL                   DEFAULT TRUE
);

CREATE TABLE login_data(
    id             INTEGER PRIMARY KEY    REFERENCES     users(id)      NOT NULL,
    username       VARCHAR(256)           NOT NULL,
    password       VARCHAR(256)           NOT NULL
);

CREATE TABLE consultations(
    id            INTEGER PRIMARY KEY  DEFAULT NEXTVAL('consultations_id_seq'),
    title         VARCHAR(256)         NOT NULL,
    ddate         DATE                 NOT NULL,
    start_time    TIME                 NOT NULL,
    end_time      TIME                 NOT NULL,
    duration      INTEGER              NOT NULL,
    capacity      INTEGER              NOT NULL,
    type          VARCHAR(20)          NOT NULL,
    place         VARCHAR(100)         NOT NULL,
    room          VARCHAR(50)          NOT NULL,
    description   TEXT,
    teacher_id    INTEGER REFERENCES   users(id) NOT NULL
);

CREATE TABLE registered_consultations(
    consultation_id    INTEGER REFERENCES     consultations(id) NOT NULL,
    student_id         INTEGER REFERENCES     users(id) NOT NULL,
    registered         TIMESTAMP              DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY        (consultation_id, student_id)
);

CREATE TABLE messages(
    consultation_id    INTEGER REFERENCES     consultations(id) NOT NULL,
    student_id         INTEGER REFERENCES     users(id) NOT NULL,
    teacher_id         INTEGER REFERENCES     users(id) NOT NULL,
    time               TIMESTAMP              NOT NULL,
    message            TEXT                   NOT NULL,
    PRIMARY KEY        (consultation_id, student_id, teacher_id)
);