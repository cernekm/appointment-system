INSERT INTO users(name, surname, date_of_birth, phone, email, type)
    VALUES (
        'Frederik',
        'Müller',
        TO_DATE('26.5.1994', 'dd.mm.yyyy'),
        '+421900123111',
        'muller@fit.vutbr.cz',
        'teacher'
);

INSERT INTO users(name, surname, date_of_birth, phone, email, type)
    VALUES (
        'Martin',
        'Černek',
        TO_DATE('16.11.1993', 'dd.mm.yyyy'),
        '+421915435746',
        'xcerne01@stud.fit.vutbr.cz',
        'student'
);

INSERT INTO login_data(id, username, password)
    VALUES (
        2,
        'martas',
        'a'
);

INSERT INTO login_data(id, username, password)
    VALUES (
        1,
        'frodo',
        'q'
);