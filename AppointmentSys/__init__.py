import eventlet
eventlet.monkey_patch()

from flask import Flask
from flask.ext.login import LoginManager
from flask_socketio import SocketIO, emit

app = Flask(__name__)
app.config["SECRET_KEY"] = "AppointmentSystemSecretKey"
app.debug = True

login_manager = LoginManager()
login_manager.init_app(app)

socketio = SocketIO(app)

@socketio.on('my broadcast event', namespace='/chat')
def test_connect(message):
    print message
    emit('my response', {'data': message['data'], 'user':message['user']}, broadcast=True)

import AppointmentSys.views
