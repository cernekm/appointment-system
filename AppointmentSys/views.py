from AppointmentSys import app, login_manager
from flask import render_template, flash, redirect, url_for, request, session
from flask.ext.login import current_user, login_user, logout_user
from flask.ext.security import login_required
from datetime import timedelta
import traceback
import time
from datetime import datetime, timedelta
from pprint import pprint as pp
import json

from decorators import *
from models import User
import config

db_connection_read_only = create_db_connection(config.database)
db_connection = create_db_connection(config.database, read_only = False)




@login_manager.user_loader
def load_user(id):
    return User.get(id)

@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect(url_for("login"))

@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=10)

@app.route('/login')
def login():
    return render_template("login.html")

@app.route('/login/check', methods=["POST"])
def login_check():
    user = User.get(request.form["inputUsername"])
    if (user and user.password == request.form["inputPassword"]):
        remember = request.form.get("rememberMe", None) == "on"
        print "remember", remember
        login_user(user,remember)
        return redirect(url_for("index"))
    else:
        flash("error-wrong-username-or-password")
        return redirect(url_for("login"))

@app.route('/logout', methods=["GET"])
def logout():
    logout_user()
    return redirect(url_for("login"))



@app.route('/')
@login_required
def index():
    return render_template("index.html", name = current_user.name, surname = current_user.surname, role = current_user.role)


@app.route('/create-consultation', methods=["GET"])
@login_required
def create():
    return render_template("create_consultation.html", name = current_user.name, surname = current_user.surname, role = current_user.role)

@app.route('/create-consultation/finish', methods=["POST"])
@login_required
@db_connection
def create_finish(cur):
    data = {
        "title": request.form.get("inputTitle",""),
        "place": request.form.get("inputPlace", ""),
        "capacity": int(request.form.get("inputCapacity", 0)),
        "room": request.form.get("inputRoom", ""),
        "ddate": request.form.get("inputDate", None),
        "duration": int(request.form.get("inputDuration", 0)),
        "description": request.form.get("inputDescription", ""),
        "type": request.form.get("inputType", "personally"),
        "teacher_id": current_user.db_id
    }
    duration = timedelta(minutes = int(request.form.get("inputDuration", 0)))
    start_time = datetime.strptime("%s %s" % (request.form.get("inputDate", None), request.form.get("inputTime", None)), "%d/%m/%Y %H:%M")
    repeats = int(request.form.get("inputRepeats", 0))
    break_duration = timedelta(minutes = int(request.form.get("inputBreak", 0)))
    print start_time
    for _ in range(repeats):
        print start_time, start_time + duration
        data.update({
            "start_time": start_time.strftime("%H:%M"),
            "end_time": (start_time + duration).strftime("%H:%M"),
        })
        cur.execute("""INSERT INTO consultations (title,
                                                  ddate,
                                                  start_time, 
                                                  end_time, 
                                                  duration,
                                                  capacity,
                                                  type,
                                                  place,
                                                  room,
                                                  description,
                                                  teacher_id) 
                        VALUES ( %(title)s,
                                 TO_DATE(%(ddate)s, 'dd/mm/yyyy'),
                                 %(start_time)s,
                                 %(end_time)s,
                                 %(duration)s,
                                 %(capacity)s, 
                                 %(type)s,
                                 %(place)s,
                                 %(room)s,
                                 %(description)s,
                                 %(teacher_id)s)""", data)
        start_time += duration + break_duration
    return redirect(url_for("index"))



@app.route('/search-consultations')
@login_required
@db_connection_read_only
def search(cur):
    cur.execute("SELECT * FROM users WHERE type = 'teacher' ORDER BY surname ASC")
    data = []
    for teacher in cur.fetchall():
        teacher["date_of_birth"] = teacher["date_of_birth"].strftime("%d/%m/%Y")
        data.append(teacher)
    return render_template("search_consultations.html", name = current_user.name, surname = current_user.surname, data = data, role = current_user.role)


@app.route('/show-consultations/<int:teacher_id>')
@login_required
@db_connection_read_only
def show(cur, teacher_id):
    cur.execute("SELECT *,  ( select  COUNT(*) from registered_consultations where consultation_id = id ) as reserved  FROM consultations WHERE teacher_id = %s ORDER BY ddate ASC", (teacher_id,))
    data = []
    for cons in cur.fetchall():
        cons["ddate"] = cons["ddate"].strftime("%d/%m/%Y")
        cons["start_time"] = cons["start_time"].strftime("%H:%M")
        cons["end_time"] = cons["end_time"].strftime("%H:%M")
        data.append(cons)
    return render_template("show_consultations.html", name = current_user.name, surname = current_user.surname, data = data, json_data = json.dumps(data), role = current_user.role)



@app.route('/edit-consultation-1')
@login_required
def edit1():
    return render_template("edit_consultations1.html", name = current_user.name, surname = current_user.surname, role = current_user.role)




@app.route('/edit-consultation-2')
@login_required
def edit2():
    return render_template("edit_consultations2.html", name = current_user.name, surname = current_user.surname, role = current_user.role)

@app.route('/show-table')
@login_required
def reserve():
    if current_user.role == "teacher":
        return render_template("reserve_consultation1.html", name = current_user.name, surname = current_user.surname, role = current_user.role)
    else:
        return render_template("reserve_consultation.html", name = current_user.name, surname = current_user.surname, role = current_user.role)


@app.route('/current-consultation')
@login_required
def online():
    return render_template("online.html", name = current_user.name, surname = current_user.surname, role = current_user.role)
