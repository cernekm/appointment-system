# -*- coding: utf-8 -*-
from flask.ext.login import UserMixin
import config
from pprint import pprint as pp

from decorators import *
db_connection = create_db_connection(config.database)

class UserNotFoundError(Exception):
    pass


class User(UserMixin):

    def __init__(self, id):
        user = self.load(id)
        if not user:
            raise UserNotFoundError()
        self.id = user["username"] # username
        self.password = user["password"]
        self.db_id = user["id"] # id in DB:
        self.role = user["type"]
        self.name = user["name"]
        self.surname = user["surname"]

    @db_connection
    def load(cur, self, id):
        cur.execute("SELECT * FROM users, login_data WHERE users.id = login_data.id AND username = %s AND users.active = 't'", (id,))
        user = cur.fetchone()
        return user

    @classmethod
    def get(self_class, id):
        try:
            return self_class(id)
        except UserNotFoundError:
            return None
