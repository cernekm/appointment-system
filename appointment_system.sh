#! /bin/bash
# /etc/init.d/gunicorn
#

source /srv/venv/bin/activate
daemon=/srv/venv/bin/gunicorn
pid=/tmp/appointment_system.pid
cd /srv/appointment-system
args="--config /srv/appointment-system/as_gunicorn.py.ini run:app"

case "$1" in
   start)
       echo "Starting gunicorn"
       start-stop-daemon -p $pid --start --background --exec $daemon -- $args
       ;;
   stop)
       echo "Stopping script gunicorn"
       start-stop-daemon --signal INT -p $pid --stop $daemon -- $args
       ;;
   *)
       echo "Usage: ./hospital.sh {start|stop}"
       exit 1
   ;;
esac

exit 0
